function [y1, y2, wn, w1, w2, w3, absH1, absH2, absH3, f, g] = ex2(t)

syms s
s = tf('s');
% Functia de transfer
H = 2 / (s^2 + 2*s + 2);
subplot(2,2,1);
step(H);

% LaPlace^-1(Y*u)
y1 = -exp(-t).*cos(t)-exp(-t).*sin(t)+1;

% u(t) = t*1(t)
u = t.*heaviside(t);
y2 = lsim(H,u,t);

% Pulsatia naturala wn
wn = sqrt(2);
% w1 < wn
w1 = 0;
% w1 = wn
w2 = wn;
% w3 > wn
w3 = 3;

% Raspunsul la intrari armonice ui
subplot(2,2,2);
u1 = sin(w1*t);
lsim(H,u1,t);
hold on;
u2 = sin(w2*t);
lsim(H,u2,t);
hold on;
u3 = sin(w3*t);
lsim(H,u3,t);

% Calcul Hi si Modul Hi
H1 = 2/((i*w1)^2 + 2*i*w1 + 2);
absH1 = abs(H1);
H2 = 2/((i*w2)^2 + 2*i*w2 + 2);
absH2 = abs(H2);
H3 = 2/((i*w3)^2 + 2*i*w3 + 2);
absH3 = abs(H3);

w = logspace(-2,2,100);
H4 = 2/(-(w').^2 + 2*i*w' + 2);
% f = |H(jw)|
f = abs(H4);
% g = argH(jw)
g = angle(H4);

subplot(2,2,3);
hold on
semilogx(w,H4);

end