function [Y, y, H, px, py, y1, y2, y3, yp, yt] = ex3(t)

    s = tf('s');
    % Functia de transfer pentru iesire
    Y = (-s^2 - 6*s - 1)/(s*(s^2 + 4*s + 20));
    subplot(2,2,1);
    impulse(Y);
    
    % LaPlace^-1(Y*u)
    y = (-19/20)*exp(-2*t).*cos(4*t) - (39/40)*exp(-2*t).*sin(4*t) - 1/20;

    % H(s) in conditii initiale nule
    H = (2*s-1)/(s^2 + 4*s + 20);
    
    % Polii
    px = [-2 -2];
    py = [-4 4];
    
    % Iesirea sistemului la intrarea u1
    y1 = ((1/20)*exp(-2*t).*cos(4*t) + (21/40)*exp(-2*t).*sin(4*t) - 1/20)';
    subplot(2,2,2);
    plot(y1);
    
    % Iesirea sistemului la intrarea u2
    y2 = ((-11/100)*exp(-2*t).*cos(4*t) - (17/400)*exp(-2*t).*sin(4*t) - t/20 + 11/100)';
    subplot(2,2,3);
    plot(y2);
    
    % Iesirea sistemului la intrarea u3
    y3 = ((-61/265)*exp(-2*t).*cos(4*t) + (321/530)*exp(-2*t).*sin(4*t) + (61/265)*cos(3*t) - (78/265)*sin(3*t))';
    subplot(2,2,4);
    plot(y3);
    
    % Componenta permanenta
    yp = - 1/20;
    
    % Componenta tranzitorie
    yt = (-19/20)*exp(-2*t).*cos(4*t) - (39/40)*exp(-2*t).*sin(4*t);

end