function [H, h, y3, y4, y5] = ex1( t, w, N )

% Functia de transfer
H = tf(1,[1 1]);

% Raspunsul la impuls
h = impulse(H);

% Intrare de tipul e^(j*w*t)
x3 = exp(i*w*t);
% Raspuns la x3
y3 = lsim(H, x3, t);

% Intrare de tipul cos(t)
x4 = cos(t);
% Raspuns la x4
y4 = lsim(H, x4, t);

% Intrare de tipul sum[k=1 N](k*cos(2*k*pi*t)) N>10
% Calculam suma
sum = 0;
for j = 1 : N
    sum = sum + j*cos(2*j*pi*t);
end

% Intrarea x5
x5 = sum;
% Raspuns la x5
y5 = lsim(H,x5,t);
end